using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

public class mainScript : MonoBehaviour
{

  public  ARRaycastManager arraycast;
  public  GameObject spawning;

  public GameObject debug;


  public List<ARRaycastHit> listahitow = new List <ARRaycastHit>();

    void Start()
    {

    }

   public void displayDebug(string cosnaekranie) {
      debug.GetComponent<Text>().text = cosnaekranie;
    }

    // Update is called once per frame
    void Update()
    {
      if(Input.touchCount>0) {
        var touch = Input.GetTouch(0);
        if(touch.phase == TouchPhase.Ended) {
          if(Input.touchCount==1) {
             Vector2 position = Input.GetTouch(0).position;
             if(arraycast.Raycast(position, listahitow,  TrackableType.PlaneWithinPolygon)) {
               var localhit = listahitow[0].pose.position;
               Instantiate(spawning, listahitow[0].pose.position, listahitow[0].pose.rotation);
               displayDebug("hit:" +localhit.x + "," + localhit.y + ","+localhit.z);
             }


          }
        }
      }
    }
}
